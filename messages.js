
const MSGS = {
    'CATEGORY404' : 'Caterogia nao encontrada',
    'CONTENT404': 'Conteudo não encontrada', 
    'GENERIC_ERROR': 'Erro!',
    'INVALID_TOKEN' : 'Token nao enviado',
    'PASSWORD_INVALID' : 'Senha incorreta', 
    'PRODUCT404' : 'Produto nao encontrado',
    'REQUIRED_PASSWORD' : 'Por favor insira sua senha',
    'USER404' : 'Usuario nao encontrado',
    'VALID_EMAIL' : 'Por favor insira, email valido',
    'WITHOUT_TOKEN' : 'Token nao enviado'
  
}

module.exports = MSGS